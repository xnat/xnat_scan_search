# Search XNAT using cURL

This project contains an example xml (search_conditions.xml) which specifies conditions for searching some xnat-projects which contain fields with specific scan-types and -parameters. This file is needed to make a cURL Query for searching, using the defined conditions. Additionally it contains a python script for sorting your results.

## Files
* search_conditions.xml *an example xml file containing the search conditions. This file will be sent with a cURL-command*
* sort_results.py *a python-script for sorting the search-results, if you dont alter it, it will sort the results by project-name*

## Prerequisites
* python3
* cURL (mostly pre-installed on Linux-systems)

## How to use
* alter the search_conditions.xml for your demands:
   * search_field *here you can define the fields who will display from the resulting projects*
   * you can find the id and header of a specific searchfield by quering for it using the XNATRestClient
   * query for available data types: *XNATRestClient -host http://yourHOST -u yourUSER -p yourPASSWORD -remote "/data/search/elements?format=json" -m GET*
   * query the fields for a specific data type (for example mrSessionData): *XNATRestClient -host http://yourHOST -u yourUSER -p yourPASSWORD -remote "/data/search/elements/xnat:mrSessionData?format=json" -m GET*
   * search_where method=OR *here you can use OR or AND*
   * criteria *here you define your criteria, chained by your search method*
   * available comparision types: `LIKE`, `=`, `&lt;` (less than: <), `&gt;` (greater than >), `&lt;=` (greater than or equal), `&lt;=` (less than or equal)

* send your cURL-query:
   * curl -X POST -d @search_conditions.xml -u yourUSER:yourPASSWORD "http://yourHOST/data/search?format=json" | python3 sort_results.py

## Original Source
* [xnat-documentation] (https://wiki.xnat.org/display/XNAT/Query+the+XNAT+Search+Engine+with+REST+API "https://wiki.xnat.org/display/XNAT/Query+the+XNAT+Search+Engine+with+REST+API")
